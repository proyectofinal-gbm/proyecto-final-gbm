﻿Imports System
Imports System.Data.SqlClient

Public Class Datos
    Inherits System.Web.UI.Page
    Implements IConexionBd


    Private _conexion As ConexionBd = New ConexionBd()

    Public Sub Insertar() Implements IConexionBd.Insertar
        Throw New NotImplementedException()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    ''' <summary>
    ''' Boton para registrar en la base de datos
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub BtnRegistrar_Click(sender As Object, e As EventArgs)
        Dim nombre As String = Me.txtNombre.Text
        Dim apellido As String = Me.txtApellido.Text
        Dim edad As String = Me.txtEdad.Text
        Dim nacionalidad As String = Me.txtNacionalidad.Text

        InsertarDatos(nombre, apellido, edad, nacionalidad)

        limpiar()

    End Sub

    Private Sub limpiar()
        Me.txtNombre.Text = ""
        Me.txtApellido.Text = ""
        Me.txtEdad.Text = ""
        Me.txtNacionalidad.Text = ""
    End Sub

    Public Sub InsertarDatos(ByVal nombre1 As String, ByVal apellido1 As String, ByVal edad1 As String, ByVal nacionalidad1 As String)

        _conexion.SqlConnection.Open()
        Dim sqlCommand As SqlCommand = Me._conexion.SqlConnection.CreateCommand()
        sqlCommand.CommandText = "INSERT INTO DATOS VALUES(@NOMBRE, @APELLIDO, @EDAD, @NACIONALIDAD)"
        sqlCommand.Parameters.AddWithValue("@NOMBRE", nombre1)
        sqlCommand.Parameters.AddWithValue("@APELLIDO", apellido1)
        sqlCommand.Parameters.AddWithValue("@EDAD", edad1)
        sqlCommand.Parameters.AddWithValue("@NACIONALIDAD", nacionalidad1)



        sqlCommand.ExecuteNonQuery()



        Me._conexion.SqlConnection.Close()



        MsgBox("Registro de USUARIO Creado", MsgBoxStyle.Information, "INFO")

    End Sub

    Public Function Actualizar() As Boolean Implements IConexionBd.Actualizar
        Throw New NotImplementedException()
    End Function

    Public Function Eliminar() As Boolean Implements IConexionBd.Eliminar
        Throw New NotImplementedException()
    End Function

    Public Function Seleccionar() As Object Implements IConexionBd.Seleccionar
        Throw New NotImplementedException()
    End Function
End Class