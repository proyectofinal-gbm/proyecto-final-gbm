﻿Public Interface IConexionBd

    ''' <summary>
    ''' Metodo usado para insertar un registro en la base de datos.
    ''' </summary>
    Sub Insertar()

    ''' <summary>
    ''' Metodo usado para actualizar un registro en la base de datos.
    ''' </summary>
    ''' <returns>True si la operacion fue exitosa.</returns>
    Function Actualizar() As Boolean

    ''' <summary>
    ''' Metodo usado para eliminar un registro en la base de datos.
    ''' </summary>
    ''' <returns>True si la operacion fue exitosa.</returns>
    Function Eliminar() As Boolean

    ''' <summary>
    ''' Metodo usado para insertar un objeto en la base de datos.
    ''' </summary>
    ''' <returns>True si la operacion fue exitosa.</returns>
    Function Seleccionar() As Object

End Interface
