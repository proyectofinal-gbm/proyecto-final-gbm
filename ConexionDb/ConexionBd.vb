﻿Imports System.Data.SqlClient

''' <summary>
''' Clase usada para realizar la conexion con la bd.
''' </summary>
Public Class ConexionBd
    ''' <summary>
    ''' Objeto manejador de la conexion con sql server.
    ''' </summary>
    Private _connection As SqlConnection

    ''' <summary>
    ''' Constructor vacío.
    ''' </summary>
    Public Sub New()
        CadenaConexionBd = ConfigurationManager.ConnectionStrings("LocalServer").ConnectionString
    End Sub

    ''' <summary>
    ''' Gets or sets Cadena de conexion de la bd.
    ''' </summary>
    Public Property CadenaConexionBd As String

    ''' <summary>
    ''' Gets or sets Objeto que contiene la conexión con Sql Server.
    ''' </summary>
    Public ReadOnly Property SqlConnection As SqlConnection
        Get
            If _connection Is Nothing Then _connection = New SqlConnection(CadenaConexionBd)
            Return _connection
        End Get
    End Property
End Class
