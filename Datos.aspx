﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Datos.aspx.vb" Inherits="WebAplicattionGBM.Datos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
    <div  class="jumbotron">
        <img src="http://placekitten.com/150/150"/>
        <h1>DATOS USUARIO</h1>
        <p class="lead">Ingrese sus Datos: </p>
    </div>

    <div class="textos">
        <asp:Label ID="lblNombre" runat="server" Text="Label">Nombre: </asp:Label>
        <asp:TextBox class="text" ID="txtNombre" runat="server" autocomplete="off"></asp:TextBox>
    </div>
    <div class="textos">
        <asp:Label ID="lblApellido" runat="server" Text="Label">Apellido:  </asp:Label>
        <asp:TextBox class="text" ID="txtApellido" runat="server" autocomplete="off"></asp:TextBox>
    </div>
  
    <div class="textos">
        <asp:Label ID="lblEdad" runat="server" Text="Label">Edad: </asp:Label>
        <asp:TextBox class="text" ID="txtEdad" runat="server" autocomplete="off"></asp:TextBox>
    </div>
   
     <div class="textos">
        <asp:Label ID="lblNacionalidad" runat="server" Text="Label">Nacionalidad: </asp:Label>
        <asp:TextBox class="text" ID="txtNacionalidad" runat="server" autocomplete="off"></asp:TextBox>
    </div>
    <div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                 <asp:Button class="btn btn-primary btn-lg" ID="BtnRegistrar" runat="server" Text="Registrar" OnClick="BtnRegistrar_Click"/>
            </ContentTemplate>        
        </asp:UpdatePanel>
    </div>
     

</asp:Content>
